*Settings*
Documentation                   Cadastro de produtos na nova tela

*Variables*
${groupName}                    outback
${username}                     quality
${password}                     qa1234

*Keywords*
Criação de novo produto
#Login
    Fill text                   id=groupName                ${groupName}    
    Fill text                   id=username                 ${username}
    Fill text                   id=password                 ${password}
      
    Click                       id=submitButton
    
    Click                       text=Outback
    Click                       text=Zak

#Sleep usado exclusivamente para que o range de ORGANIZAÇÃO esteja ativo para o clique
   Sleep                        2

#Acesso a aba de ORGANIZAÇÃO para criar um novo grupo fiscal
    Click                       text=ORGANIZAÇÃO
    Click                       text=Cardápio
    Click                       text=Produtos
    Click                       text=CRIAR PRODUTO

#Criação do produto

#Nome do produto
    Fill text                   id=name                       Chá de hibisco com maça
#Código de barras
    Fill text                   id=barcodeFixed               7891098041203
#Nome para cozinha
    Fill text                   id=nameOnApp                  Chá de hibisco com maça!
#Descrição do produto
    Fill text                   id=description                Chá exclusivo de hibisco com maça e toque de amora.
#Abrindo o modal do dropdown de NCM
    Click                       id=#zakDataListInput
#Seleção de uma das opções de origem 
    Fill text                   id=#zakDataListInput          21069030 - COMPLEMENTOS ALIMENTARES
    Click                       xpath=//li[contains(text(),'21069030 - COMPLEMENTOS ALIMENTARES')]
#CEST
    Fill text                   id=cest                       2100900
#Selecionar restaurante para habilitar no PDV
    Fill text                   xpath=//*[contains(text(),'Restaurante(s)')]            Zak
    Click                       text=Zak           
#Selecionar categoria 
    Click                       xpath=//*[contains(text(),'Selecione ou digite a categoria')]
    Click                       xpath=//*[contains(text(),'Accountability')]
#Selecionar subcategoria 
    Click                       xpath=//span[contains(text(),'Subcategoria')]
    Click                       xpath=//*[contains(text(),'Outros')]
#Preço
    Fill text                   id=Preço base-inputmoney                                25,50
    Click                       xpath=//span[contains(text(),'Aplicar Preço')]
#Selecionar restaurante
    