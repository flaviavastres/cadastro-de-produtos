*Settings*
Documentation                Cadastro de produtos na nova tela

Library                      Browser

Resource                     actions/source.robot

*Keywords*
Start Session
    New Browser              chromium             headless=false
    New Context              viewport={'width': 1366, 'height': 768}    
    New Page                 https://zakpay-dash-staging-2.web.app/login
    Get Text                 h1                   contains                Gerenciamento de restaurantes